using System;

namespace HanzoNetwork
{
	public class Match : ICloneable
	{
		public int Id {get; set;}

		public MatchStatus Status {get; set;} = MatchStatus.Empty;

		public User User1 {get; set;}

		public User User2 {get; set;}

		public int MatchmakingStrength {get; set;}

		public int CheckedIn {get; set;} = 0;

		public Match(int id)
		{
			Id = id;
		}

		public object Clone()
	    {
	        return this.MemberwiseClone();
	    }

	    public int Contains(User user)
	    {
	    	if (User1 != null && User1.Id == user.Id)
	    	{
	    		return 1;
	    	}
	    	if (User2 != null && User2.Id == user.Id)
	    	{
	    		return 2;
	    	}
	    	return 0;
	    }
	}
}

public enum MatchStatus
{
	Empty,
    HostWaiting,
    Matchmaking,
    Loading,
    Playing,
    //Reviewing,
    //Finished,
}