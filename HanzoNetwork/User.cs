using System;
using System.Net.Sockets;

namespace HanzoNetwork
{
	public class User
	{
		public int Id {get; set;}

		public Socket Handler {get; set;}

		public long ConnectionIndex {get; set;} = -1;

		public bool Connected {get; set;} = true;

		public int IdleCount {get; set;} = 0;

		public UserStatus Status {get; set;}

		public string Username {get; set;}

		public int Mmr {get; set;}

		public int MatchmakingStrength {get; set;} = Int32.MaxValue;

		public User(int id, Socket handler)
		{
			Random random = new Random();
			Id = id;
			Handler = handler;
		}

		public int GetMatchmakingStrength(User opponent)
		{
			return Math.Abs(Mmr - opponent.Mmr);
		}
	}
}

public enum UserStatus
{
	Waiting,
	Hosting,
	Matchmaking,
	Playing,
}