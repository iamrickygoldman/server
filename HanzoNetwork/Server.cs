using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Collections.Generic;
using System.Collections.Concurrent;

using HanzoHelper;

namespace HanzoNetwork
{
	public class Server
	{
		public const int NULL_INT = Int32.MinValue;

		private const string END = "`";
		private const int PING_TIME = 5000; // 5 seconds
		private const int MAX_IDLE_COUNT = 120; // 10 minutes

		public bool Testing {get; set;} = false;

		public bool Shutdown {get; set;} = false;

		private TcpListener Listener {get; set;}

		private bool Listening {get; set;} = true;

		private List<string> Ipv4Blacklist {get; set;}
		private List<string> Ipv6Blacklist {get; set;}

		private int connectionCount = 0;
		private int testCount = 0;
		private ConcurrentDictionary<int,Socket> AllConnections {get; set;}
		private ConcurrentDictionary<int,User> UserConnections {get; set;}

		private ConcurrentDictionary<int,User> UsersMatchmaking {get; set;}
		private Dictionary<int,Match> MatchesMatchmaking {get; set;}

		private ConcurrentDictionary<int,Match> CurrentMatches {get; set;}
		private int matchCounter = 0;

		private List<int> QueuedDisconnects {get; set;}

		public Server()
		{
			Database db = new Database();
			db.EndAllConnections();

			Ipv4Blacklist = db.GetBlacklist("ipv4");
			Ipv6Blacklist = db.GetBlacklist("ipv6");

			Config config = new Config();
			IPAddress address = config.GetNetworkAddress();
			int port = config.GetNetworkPort();

			AllConnections = new ConcurrentDictionary<int,Socket>();
			UserConnections = new ConcurrentDictionary<int,User>();

			UsersMatchmaking = new ConcurrentDictionary<int,User>();
			MatchesMatchmaking = new Dictionary<int,Match>();

			CurrentMatches = new ConcurrentDictionary<int,Match>();

			QueuedDisconnects = new List<int>();

			Listener = new TcpListener(address, port);
			Listener.Start();
		}

		public void StartMatchmaking()
		{
			uint count = 0;
			while (!Shutdown)
			{
				if (count %1 == 0)
				{
					KeepAlive();
				}
				Matchmake();
				Thread.Sleep(PING_TIME);
				count++;
			}
			GracefulShutdown();
		}

		public void KeepAlive()
		{
			Emit("#");
		}

		public void Matchmake()
		{
			List<int> userKeys = new List<int>(UsersMatchmaking.Keys);
			for (int i = 0; i < userKeys.Count; i++)
			{
				Console.WriteLine("{0} is matchmaking.", UsersMatchmaking[userKeys[i]].Username);
			}
			if (UsersMatchmaking.Count < 2)
			{
				return;
			}
			//Console.WriteLine("Matching...");
			// Match users
			Random random = new Random();
			List<int> matched = new List<int>();
			userKeys.Sort((x, y) => UsersMatchmaking[x].Mmr.CompareTo(UsersMatchmaking[y].Mmr));
			for (int i = 0; i < userKeys.Count-1; i++)
			{
				if (matched.Contains(i))
				{
					continue;
				}
				int range = 5;
				int max = range;
				int min = -1 * range;
				if (userKeys.Count - i <= range)
				{
					max = userKeys.Count - i - 1;
				}
				if (i < range)
				{
					min = -1 * i;
				}
				int diff;
				do {
					diff = random.Next(min, max+1);
				} while (diff == 0);
				int partner = i + diff;
				if (!UsersMatchmaking.ContainsKey(userKeys[i]) || !UsersMatchmaking.ContainsKey(userKeys[partner]))
				{
					continue;
				}
				User user1 = UsersMatchmaking[userKeys[i]];
				User user2 = UsersMatchmaking[userKeys[partner]];

				int matchmakingStrength = user1.GetMatchmakingStrength(user2);
				if (matchmakingStrength < user1.MatchmakingStrength && matchmakingStrength < user2.MatchmakingStrength)
				{
					user1.MatchmakingStrength = matchmakingStrength;
					user2.MatchmakingStrength = matchmakingStrength;
					int c = 0;
					List<int> matchKeys = new List<int>(MatchesMatchmaking.Keys);
					foreach (int key in matchKeys)
					{
						Match tmp = MatchesMatchmaking[key];
						if (tmp.Contains(user1) > 0 || tmp.Contains(user2) > 0)
						{
							MatchesMatchmaking.Remove(key);
							c++;
						}
						if (c == 2)
						{
							break;
						}
					}
					Interlocked.Increment(ref matchCounter);
					Match match = new Match(matchCounter);
					Console.WriteLine("Potential match {5} between {0}: {3} and {1}: {4} with strength {2}", user1.Username, user2.Username, matchmakingStrength, user1.Mmr, user2.Mmr, match.Id);
					match.User1 = user1;
					match.User2 = user2;
					match.Status = MatchStatus.Matchmaking;
					match.MatchmakingStrength = matchmakingStrength;
					MatchesMatchmaking.Add(match.Id, match);
				}
			}

			// Move qualifying matches to real matches
			List<int> keys = new List<int>(MatchesMatchmaking.Keys);
			foreach (int key in keys)
			{
				Match tmp = MatchesMatchmaking[key];
				if (tmp.MatchmakingStrength < 100)
				{
					MatchesMatchmaking.Remove(key);
					if (UsersMatchmaking.ContainsKey(tmp.User1.Id))
					{
						UsersMatchmaking.TryRemove(tmp.User1.Id, out User u);
						Console.WriteLine("{0} removed from matchmaking", tmp.User1.Username);
					}
					if (UsersMatchmaking.ContainsKey(tmp.User2.Id))
					{
						UsersMatchmaking.TryRemove(tmp.User2.Id, out User u);
						Console.WriteLine("{0} removed from matchmaking", tmp.User2.Username);
					}
					tmp.Status = MatchStatus.Loading;
					tmp.User1.MatchmakingStrength = Int32.MaxValue;
					tmp.User2.MatchmakingStrength = Int32.MaxValue;
					tmp.User1.Status = UserStatus.Playing;
					tmp.User2.Status = UserStatus.Playing;
					CurrentMatches.TryAdd(tmp.Id, tmp);
					Emit("#m" + PackString(tmp.Id, "M") + PackString(tmp.User1.Id, "1") + PackString(tmp.User2.Id, "2") + PackString(tmp.User1.Username, "A") + PackString(tmp.User2.Username, "B") + PackString(tmp.User1.Mmr, "a") + PackString(tmp.User2.Mmr, "b"), tmp);
					Console.WriteLine("Creating match between {0} and {1}", tmp.User1.Username, tmp.User2.Username);
				}
				else
				{
					Console.WriteLine("Potential match between {0} and {1} too weak: {2}", tmp.User1.Username, tmp.User2.Username, tmp.MatchmakingStrength);
					tmp.MatchmakingStrength -= 50;
				}
			}
		}

		public async void StartListening()
		{
			while (Listening)
			{
				TcpClient client = await Listener.AcceptTcpClientAsync().ConfigureAwait(false);
				new Thread(() => HandleClient(client)).Start();
			}
			try {
				Listener.Stop();
			} catch (Exception e) {
				Console.WriteLine(e.ToString());
			}
		}

		protected void HandleClient(TcpClient client)
		{
			string data = null;
			byte[] bytes = new Byte[1024];
			Database handlerDb = new Database();

			try {
				Socket handler = client.Client;
				string tmpIp = ((IPEndPoint)handler.RemoteEndPoint).Address.ToString();
				//int tmpPort = ((IPEndPoint)handler).Port;
				handler.ReceiveTimeout = PING_TIME * 2;
				if (Testing)
				{
					handler.ReceiveTimeout = 0;
				}
				User user = new User(-1, handler);
				Interlocked.Increment(ref connectionCount);
				int tmpCount = connectionCount;
				AllConnections.TryAdd(tmpCount, handler);
				long connectionIndex = handlerDb.InsertConnection(((IPEndPoint)handler.RemoteEndPoint).Address, ((IPEndPoint)handler.RemoteEndPoint).Port);
				user.ConnectionIndex = connectionIndex;

				while (Listening)
				{
					if (QueuedDisconnects.Contains(tmpCount) || IsBlacklisted(tmpIp))
					{
						goto End;
					}
					data = null;

					//Console.WriteLine("Waiting for a connection on {0}", handler.RemoteEndPoint.ToString());

					while (true)
					{
						if (!user.Connected)
						{
							Console.WriteLine("not connected");
							goto End;
						}
						try {
							int bytesRec = handler.Receive(bytes);
							if (bytesRec == 0)
							{
								Console.WriteLine("empty bytes");
								goto End;
							}
							data += Encoding.ASCII.GetString(bytes, 0, bytesRec);
							//Console.WriteLine("Bytes received : {0}", bytesRec);
							if (data.IndexOf(END) > -1)
							{
								break;
							}
						} catch (Exception e) {
							Console.WriteLine(e.ToString());
							goto End;
						}
					}

					if (data != null)
					{
						//Console.WriteLine("Text received : {0}", data);

						int endIndex = data.IndexOf(END);
			            do
			            {
			                string chunk = data.Substring(0, endIndex + 1);
			                if (endIndex == data.Length) {
			                    break;
			                }
			                data = data.Substring(endIndex + 1);
			                endIndex = data.IndexOf(END);
			                //Console.WriteLine("Processing: {0}", chunk);
			                MessageStatus result = Process(chunk, user);
			                // Query
			                if ((int)result < 0)
			                {
			                	Console.WriteLine("Query: {0}", (int)result);
			                	user.IdleCount = 0;
			                }
			                // Success
			                else if ((int)result < 100)
			                {
			                	if ((int)result == 10 || (int)result == 90)
			                	{
			                		handlerDb.AddUserToConnection(user);
			                	}
			                	if ((int)result == 0)
			                	{
			                		if (++user.IdleCount > MAX_IDLE_COUNT)
			                		{
										Send("#d", user);
										user.IdleCount = 0;
										goto End;
			                		}
			                	}
			                	else
			                	{
			                		user.IdleCount = 0;
			                	}
			                	Console.WriteLine("Success: {0}", (int)result);
			                	if (result == MessageStatus.SuccessLogout)
			                	{
			                		goto End;
			                	}
			                }
			                // Error
			                else if ((int)result < 1000)
			                {
			                	Console.WriteLine("Error: {0}", (int)result);
								goto End;
			                }
			                // Warning
			                else
			                {
			                	Console.WriteLine("Warning: {0}", (int)result);
			                }
			            } while (endIndex > -1 && endIndex < data.Length);
			            //Console.WriteLine("Fallout. End: {0}, Length: {1}", endIndex, data.Length);
					}
					else
					{
						Console.WriteLine("null");
					}
				}
				End:

				Console.WriteLine("Closing connection on {0}", handler.RemoteEndPoint.ToString());
				AllConnections.TryRemove(tmpCount, out Socket s);
				DisconnectUser(user);
				handlerDb.EndConnection(connectionIndex);

				handler.Close();
				client.Close();
			} catch (Exception e) {
				Console.WriteLine(e.ToString());
			}
		}

		public void Close()
		{
			Listening = false;
		}

		public void Send(string message, Socket handler)
		{
			message = message + END;
			if (handler != null)
			{
				byte[] msg = Encoding.ASCII.GetBytes(message);

				try {
					handler.Send(msg);
					//Console.WriteLine("Sent : {0} to {1}", message, handler.RemoteEndPoint.ToString());
				} catch (Exception e) {
					Console.WriteLine(e.ToString());
				}
			}
			else
			{
				Console.WriteLine("Handler is null.");
			}
		}

		public void Send(string message, User user)
		{
			if (user.Connected)
			{
				Send(message, user.Handler);
			}
		}

		public void Emit(string message)
		{
			foreach (KeyValuePair<int, Socket> connection in AllConnections)
			{
				Send(message, connection.Value);
			}
		}

		public void Emit(string message, UserStatus userStatus)
		{
			foreach (KeyValuePair<int, User> user in UserConnections)
			{
				if (user.Value.Status == userStatus)
				{
					Send(message, user.Value);
				}
			}
		}

		public void Emit(string message, Match match)
		{
			Send(message, match.User1);
			Send(message, match.User2);
		}

		public void Emit(string message, Match match, User from)
		{
			if (from.Id != match.User1.Id)
			{
				Send(message, match.User1);
			}
			else if (from.Id != match.User2.Id)
			{
				Send(message, match.User2);
			}
		}

		public MessageStatus Process(string text, User user)
		{
			if (text == "#" + END)
			{
				return MessageStatus.Ping;
			}
			if (text.Length < 3)
			{
				return MessageStatus.TooShort;
			}
			text = text.Substring(0, text.Length - 1);
			char type = text[0];
			char action = text[1];
			if (user.Id == -1)
			{
				switch (type)
				{
					case '#':
						switch (action)
						{
							case 'x':
							case 'X':
							case 'i':
							case 'I':
								break;
							default:
								return MessageStatus.NoUser;
						}
						break;
					case '?':
						switch (action)
						{
							case 'X':
								break;
							default:
								return MessageStatus.NoUser;
						}
						break;
					default:
						return MessageStatus.NoUser;
				}
			}
			int matchId;
			Match tmp;
			List<int> keys;
			string pw;
			Config config;
			switch (type)
			{
				// Network Actions
				case '#':
					switch (action)
					{
						// Shutdown
						case 'X':
							pw = ExtractString(text, "X");
							config = new Config();
							if (pw == config.GetNetworkShutdownPassword())
							{
								Shutdown = true;
								return MessageStatus.Shutdown;
							}
							return MessageStatus.ShutdownWrongPassword;
						// Disconnect a Connection
						case 'x':
							pw = ExtractString(text, "X");
							config = new Config();
							if (pw == config.GetNetworkShutdownPassword())
							{
								string ip = ExtractString(text, "I");
								if (ip == "")
								{
									return MessageStatus.DisconnectNoIp;
								}
								int port = ExtractInt(text, "P");
								if (port == NULL_INT)
								{
									return MessageStatus.DisconnectNoPort;
								}
								foreach (KeyValuePair<int, Socket> connection in AllConnections)
								{
									string tmpIp = ((IPEndPoint)connection.Value.RemoteEndPoint).Address.ToString();
									int tmpPort = ((IPEndPoint)connection.Value.RemoteEndPoint).Port;
									if (ip == tmpIp && port == tmpPort)
									{
										Send("#o", connection.Value);
										QueuedDisconnects.Add(connection.Key);
										Console.WriteLine("Disconnect {0}:{1}", tmpIp, tmpPort);
										return MessageStatus.Disconnect;
									}
								}
								return MessageStatus.DisconnectNotFound;
							}
							return MessageStatus.DisconnectWrongPassword;
						// Log In
						case 'i':
							string token = ExtractString(text, "T");
							if (token == "")
							{
								Send("#o", user);
								return MessageStatus.AuthInvalidToken;
							}
							string username = ExtractString(text, "N");
							if (username == "")
							{
								Send("#o", user);
								return MessageStatus.AuthInvalidUsername;
							}
							int id = ExtractInt(text, "U");
							if (id == NULL_INT)
							{
								Send("#o", user);
								return MessageStatus.AuthInvalidId;
							}
							int mmr = ExtractInt(text, "R");
							if (mmr == NULL_INT)
							{
								Send("o", user);
								return MessageStatus.AuthInvalidMmr;
							}
							Console.WriteLine("id: {0} token: {1}", id, token);
							Database db = new Database();
							if (!db.ValidateLogin(id, token, mmr))
							{
								Send("#o", user);
								return MessageStatus.AuthWrongToken;
							}
							if (!UserConnections.ContainsKey(id))
							{
								UserConnections.TryAdd(id, user);
							}
							user.Id = id;
							user.Username = username;
							user.Mmr = mmr;
							user.Status = UserStatus.Waiting;
							user.Connected = true;
							keys = new List<int>(CurrentMatches.Keys);
							foreach (int key in keys)
							{
								int contains = CurrentMatches[key].Contains(user);
								if (contains == 1)
								{
									tmp = (Match)CurrentMatches[key].Clone();
									tmp.User1 = user;
									CurrentMatches[key] = tmp;
								}
								else if (contains == 2)
								{
									tmp = (Match)CurrentMatches[key].Clone();
									tmp.User2 = user;
									CurrentMatches[key] = tmp;
								}
							}
							return MessageStatus.SuccessLogin;
						case 'I':
							Interlocked.Increment(ref testCount);
							int idTest = testCount;
							string usernameTest = "User " + idTest;
							Random random = new Random();
							int mmrTest = random.Next(600, 2201);
							Console.WriteLine("id: {0} username: {1} MMR: {2}", idTest, usernameTest, mmrTest);
							if (!UserConnections.ContainsKey(idTest))
							{
								UserConnections.TryAdd(idTest, user);
							}
							user.Id = idTest;
							user.Username = usernameTest;
							user.Mmr = mmrTest;
							user.Status = UserStatus.Waiting;
							user.Connected = true;
							keys = new List<int>(CurrentMatches.Keys);
							foreach (int key in keys)
							{
								int contains = CurrentMatches[key].Contains(user);
								if (contains == 1)
								{
									tmp = (Match)CurrentMatches[key].Clone();
									tmp.User1 = user;
									CurrentMatches[key] = tmp;
								}
								else if (contains == 2)
								{
									tmp = (Match)CurrentMatches[key].Clone();
									tmp.User2 = user;
									CurrentMatches[key] = tmp;
								}
							}
							return MessageStatus.SuccessTestLogin;
						// Log Out
						case 'o':
							user.Connected = false;
							RemoveUserFromMatchmaking(user);
							if (RemoveUserFromConnected(user))
							{
								return MessageStatus.SuccessLogout;
							}
							return MessageStatus.FailLogout;
						// Create Host
						case 'h':
							if (user.Status != UserStatus.Waiting)
							{
								return MessageStatus.HostNotWaiting;
							}
							foreach (KeyValuePair<int, Match> curr in CurrentMatches)
							{
								if (curr.Value.Contains(user) > 0)
								{
									return MessageStatus.HostInMatch;
								}
							}
							Interlocked.Increment(ref matchCounter);
							Match match = new Match(matchCounter);
							match.User1 = user;
							match.Status = MatchStatus.HostWaiting;
							CurrentMatches.TryAdd(match.Id, match);
							user.Status = UserStatus.Hosting;
							Console.WriteLine("Creating game id: {0}", match.Id);
							BroadcastHostedMatches();
							return MessageStatus.SuccessHost;
						// Cancel Host
						case 'H':
							if (user.Status != UserStatus.Hosting)
							{
								return MessageStatus.HostNotHosting;
							}
							keys = new List<int>(CurrentMatches.Keys);
							foreach (int key in keys)
							{
								Console.WriteLine("Contains? {0} : {1}", key, CurrentMatches[key].Contains(user));
								if (CurrentMatches[key].Contains(user) == 1)
								{
									CurrentMatches.TryRemove(key, out Match m);
									user.Status = UserStatus.Waiting;
									BroadcastHostedMatches();
									return MessageStatus.SuccessHostCancel;
								}
							}
							return MessageStatus.HostMatchNotFound;
						// Join Host
						case 'j':
							if (user.Status != UserStatus.Waiting)
							{
								return MessageStatus.JoinNotWaiting;
							}
							foreach (KeyValuePair<int, Match> curr in CurrentMatches)
							{
								if (curr.Value.Contains(user) > 0)
								{
									return MessageStatus.JoinInMatch;
								}
							}
							matchId = ExtractInt(text, "M");
							if (matchId == NULL_INT)
							{
								return MessageStatus.JoinInvalidMatchId;
							}
							if (CurrentMatches.ContainsKey(matchId))
							{
								if (CurrentMatches[matchId].User1 == null)
								{
									return MessageStatus.JoinHostMissing;
								}
								if (CurrentMatches[matchId].User1.Id == user.Id)
								{
									return MessageStatus.JoinHostSame;
								}
								if (CurrentMatches[matchId].User2 != null)
								{
									return MessageStatus.JoinMatchFull;
								}
								CurrentMatches[matchId].User2 = user;
								CurrentMatches[matchId].Status = MatchStatus.Loading;
								CurrentMatches[matchId].User1.Status = UserStatus.Playing;
								user.Status = UserStatus.Playing;
								Emit("#j" + PackString(matchId, "M") + PackString(CurrentMatches[matchId].User1.Id, "1") + PackString(CurrentMatches[matchId].User2.Id, "2") + PackString(CurrentMatches[matchId].User1.Username, "A") + PackString(CurrentMatches[matchId].User2.Username, "B") + PackString(CurrentMatches[matchId].User1.Mmr, "a") + PackString(CurrentMatches[matchId].User2.Mmr, "b"), CurrentMatches[matchId]);
								BroadcastHostedMatches();
								return MessageStatus.SuccessJoin;
							}
							return MessageStatus.JoinNoMatch;
						// Join Matchmaking
						case 'm':
							if (user.Status != UserStatus.Waiting)
							{
								return MessageStatus.MatchmakingNotWaiting;
							}
							foreach (KeyValuePair<int, Match> curr in CurrentMatches)
							{
								if (curr.Value.Contains(user) > 0)
								{
									return MessageStatus.MatchmakingInMatch;
								}
							}
							if (!UsersMatchmaking.ContainsKey(user.Id))
							{
								UsersMatchmaking.TryAdd(user.Id, user);
								user.Status = UserStatus.Matchmaking;
								//Console.WriteLine("Adding {0} to matchmaking: ", user.Username);
							}
							return MessageStatus.SuccessMatchmaking;
						// Quit Matchmaking
						case 'M':
							if (user.Status != UserStatus.Matchmaking)
							{
								return MessageStatus.MatchmakingNotMatchmaking;
							}

							if (RemoveUserFromMatchmaking(user))
							{
								return MessageStatus.SuccessMatchmakingCancel;
							}
							return MessageStatus.UserNotInMatchmaking;
						// Quit Game
						case 'q':
							matchId = ExtractInt(text, "M");
							if (matchId == NULL_INT)
							{
								return MessageStatus.QuitInvalidMatchId;
							}
							if (CurrentMatches.ContainsKey(matchId))
							{
								if (CurrentMatches[matchId].Contains(user) == 1)
								{
									CurrentMatches[matchId].User1.Status = UserStatus.Waiting;
									CurrentMatches[matchId].User2.Status = UserStatus.Waiting;
									Send("#q", CurrentMatches[matchId].User2);
									CurrentMatches.TryRemove(matchId, out Match m);
									Console.WriteLine("Ending game: {0}", matchId);
								}
								else if (CurrentMatches[matchId].Contains(user) == 2)
								{
									CurrentMatches[matchId].User1.Status = UserStatus.Waiting;
									CurrentMatches[matchId].User2.Status = UserStatus.Waiting;
									Send("#q", CurrentMatches[matchId].User1);
									CurrentMatches.TryRemove(matchId, out Match m);
									Console.WriteLine("Ending game: {0}", matchId);
								}
								else
								{
									return MessageStatus.QuitNotInMatch;
								}
							}
							else
							{
								return MessageStatus.QuitNoMatch;
							}
							return MessageStatus.SuccessQuit;
						// Abort Game
						case 'a':
							matchId = ExtractInt(text, "M");
							if (matchId == NULL_INT)
							{
								return MessageStatus.AbortInvalidMatchId;
							}
							if (CurrentMatches.ContainsKey(matchId))
							{
								if (CurrentMatches[matchId].Status != MatchStatus.Loading)
								{
									return MessageStatus.AbortNotLoading;
								}
								if (CurrentMatches[matchId].Contains(user) > 0)
								{
									CurrentMatches[matchId].User1.Status = UserStatus.Waiting;
									CurrentMatches[matchId].User2.Status = UserStatus.Waiting;
									Emit("#a", CurrentMatches[matchId]);
									CurrentMatches.TryRemove(matchId, out Match m);
									Console.WriteLine("Aborting game: {0}", matchId);
								}
								else
								{
									return MessageStatus.AbortNotInMatch;
								}
							}
							else
							{
								return MessageStatus.AbortNoMatch;
							}
							return MessageStatus.SuccessAbort;
						// Load Game - Ready to Play
						case 'l':
							matchId = ExtractInt(text, "M");
							if (matchId == NULL_INT)
							{
								return MessageStatus.LoadInvalidMatchId;
							}
							if (CurrentMatches.ContainsKey(matchId))
							{
								if (CurrentMatches[matchId].Status != MatchStatus.Loading)
								{
									return MessageStatus.LoadNotLoading;
								}
								if (CurrentMatches[matchId].Contains(user) == 1)
								{
									CurrentMatches[matchId].CheckedIn |= 1;
									Send("#l", CurrentMatches[matchId].User2);
								}
								else if (CurrentMatches[matchId].Contains(user) == 2)
								{
									CurrentMatches[matchId].CheckedIn |= 2;
									Send("#l", CurrentMatches[matchId].User1);
								}
								else
								{
									return MessageStatus.LoadNotInMatch;
								}
								if (CurrentMatches[matchId].CheckedIn == 3)
								{
									CurrentMatches[matchId].Status = MatchStatus.Playing;
								}
							}
							else
							{
								return MessageStatus.LoadNoMatch;
							}
							return MessageStatus.SuccessLoad;
						default:
							return MessageStatus.NoAction;
					}
				// Queries
				case '?':
					switch (action)
					{
						// Update IP Blacklist
						case 'X':
							pw = ExtractString(text, "X");
							config = new Config();
							if (pw == config.GetNetworkShutdownPassword())
							{
								Database db = new Database();
								Ipv4Blacklist = db.GetBlacklist("ipv4");
								Ipv6Blacklist = db.GetBlacklist("ipv6");
								return MessageStatus.UpdateBlacklist;
							}
							return MessageStatus.UpdateBlacklistWrongPassword;
						// Find Match User is In
						case 'm':
							foreach (KeyValuePair<int, Match> curr in CurrentMatches)
							{
								if (curr.Value.Status == MatchStatus.Playing && curr.Value.Contains(user) > 0)
								{
									Send("#j" + PackString(curr.Value.Id, "M") + PackString(curr.Value.User1.Id, "1") + PackString(curr.Value.User2.Id, "2") + PackString(curr.Value.User1.Username, "A") + PackString(curr.Value.User2.Username, "B") + PackString(curr.Value.User1.Mmr, "a") + PackString(curr.Value.User2.Mmr, "b"), user);
									break;
								}
							}
							return MessageStatus.QueryInMatch;
						// Get Hosts
						case 'h':
							SendHostedMatches(user);
							return MessageStatus.QueryHosts;
						default:
							return MessageStatus.NoAction;
					}
				// Social Actions
				case '%':
					int targetId;
					switch (action)
					{
						// friend request
						case 'f':
							targetId = ExtractInt(text, "2");
							if (targetId == NULL_INT)
							{
								return MessageStatus.FriendRequestInvalidId;
							}
							if (UserConnections.ContainsKey(targetId))
							{
								Send("%f" + PackString(user.Id, "1") + PackString(user.Username, "A"), UserConnections[targetId]);
							}
							return MessageStatus.SuccessFriendRequest;
						// friend accept
						case 'a':
							targetId = ExtractInt(text, "2");
							if (targetId == NULL_INT)
							{
								return MessageStatus.FriendAcceptInvalidId;
							}
							if (UserConnections.ContainsKey(targetId))
							{
								Send("%a" + PackString(user.Id, "1") + PackString(user.Username, "A"), UserConnections[targetId]);
							}
							return MessageStatus.SuccessFriendAccept;
						// friend reject
						case 'r':
							targetId = ExtractInt(text, "2");
							if (targetId == NULL_INT)
							{
								return MessageStatus.FriendRejectInvalidId;
							}
							if (UserConnections.ContainsKey(targetId))
							{
								Send("%r" + PackString(user.Id, "1") + PackString(user.Username, "A"), UserConnections[targetId]);
							}
							return MessageStatus.SuccessFriendReject;
						// friend remove
						case 'F':
							targetId = ExtractInt(text, "2");
							if (targetId == NULL_INT)
							{
								return MessageStatus.FriendRemoveInvalidId;
							}
							if (UserConnections.ContainsKey(targetId))
							{
								Send("%F" + PackString(user.Id, "1") + PackString(user.Username, "A"), UserConnections[targetId]);
							}
							return MessageStatus.SuccessFriendRemove;
						// block user
						case 'e':
							targetId = ExtractInt(text, "2");
							if (targetId == NULL_INT)
							{
								return MessageStatus.BlockInvalidId;
							}
							if (UserConnections.ContainsKey(targetId))
							{
								Send("%e" + PackString(user.Id, "1") + PackString(user.Username, "A"), UserConnections[targetId]);
							}
							return MessageStatus.SuccessBlock;
						// unblock user
						case 'E':
							targetId = ExtractInt(text, "2");
							if (targetId == NULL_INT)
							{
								return MessageStatus.UnblockInvalidId;
							}
							if (UserConnections.ContainsKey(targetId))
							{
								Send("%E" + PackString(user.Id, "1") + PackString(user.Username, "A"), UserConnections[targetId]);
							}
							return MessageStatus.SuccessUnblock;
						// message user
						case 'm':
							targetId = ExtractInt(text, "2");
							if (targetId == NULL_INT)
							{
								return MessageStatus.MessageInvalidId;
							}
							if (UserConnections.ContainsKey(targetId))
							{
								Send("%m" + PackString(user.Id, "1") + PackString(user.Username, "A"), UserConnections[targetId]);
							}
							return MessageStatus.SuccessMessage;
						default:
							return MessageStatus.NoAction;
					}
				// Game Actions
				case '!':
					matchId = ExtractInt(text, "M");
					if (matchId == NULL_INT)
					{
						return MessageStatus.ActionInvalidMatchId;
					}
					if (!CurrentMatches.ContainsKey(matchId))
					{
						return MessageStatus.ActionMatchNotFound;
					}
					tmp = CurrentMatches[matchId];
					if (tmp.Status != MatchStatus.Playing)
					{
						return MessageStatus.ActionMatchNotPlaying;
					}
					switch (action)
					{
						// Test
						case 't':
							int red = ExtractInt(text, "R");
							if (red == NULL_INT || red < 0 || red > 255)
							{
								return MessageStatus.TestInvalidRed;
							}
							int green = ExtractInt(text, "G");
							if (green == NULL_INT || green < 0 || green > 255)
							{
								return MessageStatus.TestInvalidGreen;
							}
							int blue = ExtractInt(text, "B");
							if (blue == NULL_INT || blue < 0 || blue > 255)
							{
								return MessageStatus.TestInvalidBlue;
							}
							if (CurrentMatches.ContainsKey(matchId))
							{
								Emit("!t" + PackString(matchId, "M") + PackString(red, "R") + PackString(green, "G") + PackString(blue, "B"), tmp, user);
								return MessageStatus.SuccessTest;
							}
							return MessageStatus.TestNoMatch;
						default:
							return MessageStatus.NoAction;
					}
				default:
					return MessageStatus.NoType;
			}
		}

		public string ExtractString(string text, string marker)
		{
			int startIndex = text.IndexOf("{" + marker);
			if (startIndex < 0)
			{
				return "";
			}
			int stopIndex = text.IndexOf(marker + "}");
			if (stopIndex - startIndex - 2 < 1)
			{
				return "";
			}
			return text.Substring(startIndex + 2, stopIndex - startIndex - 2);
		}

		public int ExtractInt(string text, string marker)
		{
			int startIndex = text.IndexOf("{" + marker);
			if (startIndex < 0)
			{
				return NULL_INT;
			}
			int stopIndex = text.IndexOf(marker + "}");
			if (stopIndex - startIndex - 2 < 1)
			{
				return NULL_INT;
			}
			string extractedString = text.Substring(startIndex + 2, stopIndex - startIndex - 2);
			int extractedInt;
			if (Int32.TryParse(extractedString, out extractedInt))
			{
				return extractedInt;
			}
			return NULL_INT;
		}

		public string PackString(string data, string marker)
		{
			return "{" + marker + data + marker + "}";
		}

		public string PackString(int data, string marker)
		{
			return "{" + marker + data + marker + "}";
		}

		private bool RemoveUserFromConnected(User user)
		{
			if (UserConnections.ContainsKey(user.Id))
			{
				UserConnections.TryRemove(user.Id, out User u);
				return true;
			}
			return false;
		}

		private bool RemoveUserFromMatchmaking(User user)
		{
			user.Status = UserStatus.Waiting;
			List<int> keys = new List<int>(MatchesMatchmaking.Keys);
			foreach (int key in keys)
			{
				Match tmp = MatchesMatchmaking[key];
				
				if (tmp.Contains(user) > 0)
				{
					tmp.User1.MatchmakingStrength = Int32.MaxValue;
					tmp.User2.MatchmakingStrength = Int32.MaxValue;
					MatchesMatchmaking.Remove(key);
					break;
				}
			}
			if (UsersMatchmaking.ContainsKey(user.Id))
			{
				UsersMatchmaking.TryRemove(user.Id, out User u);
				Console.WriteLine("{0} removed from matchmaking", user.Username);
				return true;
			}
			return false;
		}

		private void BroadcastHostedMatches()
		{
			Emit("?h", UserStatus.Waiting);
			Emit("?h", UserStatus.Hosting);
			foreach (KeyValuePair<int, Match> curr in CurrentMatches)
			{
				if (curr.Value.Status == MatchStatus.HostWaiting)
				{
					Emit("#h" + PackString(curr.Value.Id, "M") + PackString(curr.Value.User1.Id, "1") + PackString(curr.Value.User1.Username, "A") + PackString(curr.Value.User1.Mmr, "a"), UserStatus.Waiting);
					Emit("#h" + PackString(curr.Value.Id, "M") + PackString(curr.Value.User1.Id, "1") + PackString(curr.Value.User1.Username, "A") + PackString(curr.Value.User1.Mmr, "a"), UserStatus.Hosting);
				}
			}
			Emit("?H", UserStatus.Waiting);
			Emit("?H", UserStatus.Hosting);
		}

		private void SendHostedMatches(User user)
		{
			Send("?h", user);
			foreach (KeyValuePair<int, Match> curr in CurrentMatches)
			{
				if (curr.Value.Status == MatchStatus.HostWaiting)
				{
					Send("#h" + PackString(curr.Value.Id, "M") + PackString(curr.Value.User1.Id, "1") + PackString(curr.Value.User1.Username, "A") + PackString(curr.Value.User1.Mmr, "a"), user);
				}
			}
			Send("?H", user);
		}

		public void GracefulShutdown()
		{
			Database db = new Database();
			db.EndAllConnections();
			Console.WriteLine("Graceful Shutdown!");
			Environment.Exit(Environment.ExitCode);
		}

		public void DisconnectUser(User user)
		{
			user.Connected = false;
			RemoveUserFromMatchmaking(user);
			RemoveUserFromConnected(user);
			List<int> keys = new List<int>(CurrentMatches.Keys);
			foreach (int key in keys)
			{
				Match tmp = CurrentMatches[key];
				if (tmp.Status == MatchStatus.HostWaiting)
				{
					if (tmp.User1.Id == user.Id)
					{
						Console.WriteLine("Removing Game {0}", key);
						CurrentMatches.TryRemove(key, out Match m);
						BroadcastHostedMatches();
					}
				}
			}
		}

		public bool IsBlacklisted(string ip)
		{
			//Console.WriteLine("Checking if {0} is blacklisted...", ip);
			try {
				IPAddress ipAddress = IPAddress.Parse(ip);
				if (Ipv4Blacklist != null && Ipv4Blacklist.Count > 0)
				{
					foreach (string item in Ipv4Blacklist)
					{
						try {
							IPNetwork tmpIpAddress = IPNetwork.Parse(item);
							Console.WriteLine("Comparing to {0}", tmpIpAddress.Network);
							if (tmpIpAddress.Contains(ipAddress))
							{
								return true;
							}
						} catch (Exception e) {
							Console.WriteLine(e.ToString());
							continue;
						}
					}
				}
				if (Ipv6Blacklist != null && Ipv6Blacklist.Count > 0)
				{
					foreach (string item in Ipv6Blacklist)
					{
						try {
							IPNetwork tmpIpAddress = IPNetwork.Parse(item);
							Console.WriteLine("Comparing to {0}", tmpIpAddress.Network);
							if (tmpIpAddress.Contains(ipAddress))
							{
								return true;
							}
						} catch (Exception e) {
							Console.WriteLine(e.ToString());
							continue;
						}
					}
				}
			} catch (Exception e) {
				Console.WriteLine(e.ToString());
				return true;
			}

			return false;
		}
	}

	public enum MessageStatus
	{
		// Queries < 0
		QueryHosts = -20,
		QueryInMatch = -22,

		// Success 0-99
		Ping = 0,
		Shutdown = 1,
		Disconnect = 2,
		UpdateBlacklist = 4,

		SuccessLogin = 10,
		SuccessLogout = 11,

		SuccessHost = 20,
		SuccessJoin = 21,
		SuccessMatchmaking = 22,
		SuccessQuit = 23,
		SuccessAbort = 24,
		SuccessHostCancel = 25,
		SuccessMatchmakingCancel = 26,
		SuccessLoad = 27,

		SuccessFriendRequest = 30,
		SuccessFriendAccept = 31,
		SuccessFriendReject = 32,
		SuccessFriendRemove = 33,
		SuccessBlock = 34,
		SuccessUnblock = 35,
		SuccessMessage = 36,

		SuccessTestLogin = 90,
		SuccessTest = 91,

		// Error 100-999
		ShutdownWrongPassword = 100,
		DisconnectWrongPassword = 101,
		DisconnectNoIp = 102,
		DisconnectNoPort = 103,
		UpdateBlacklistWrongPassword =  104,

		AuthInvalidId = 110,
		AuthInvalidToken = 111,
		AuthInvalidUsername = 112,
		AuthInvalidMmr = 113,

		JoinInvalidMatchId = 210,
		QuitInvalidMatchId = 230,
		AbortInvalidMatchId = 240,
		LoadInvalidMatchId = 270,

		FriendRequestInvalidId = 300,
		FriendAcceptInvalidId = 310,
		FriendRejectInvalidId = 320,
		FriendRemoveInvalidId = 330,
		BlockInvalidId = 340,
		UnblockInvalidId = 350,
		MessageInvalidId = 360,

		ActionInvalidMatchId = 400,

		TooShort = 800,
		NoUser = 801,
		NoType = 802,
		NoAction = 803,

		TestInvalidRed = 900,
		TestInvalidGreen = 901,
		TestInvalidBlue = 902,

		// Warning 1000+
		DisconnectNotFound = 1000,

		AuthWrongToken = 1100,
		FailLogout = 1110,

		HostNotWaiting = 1200,
		HostInMatch = 1201,
		HostNotHosting = 1202,
		HostMatchNotFound = 1202,
		JoinNotWaiting = 1210,
		JoinInMatch = 1211,
		JoinHostMissing = 1212,
		JoinHostSame = 1213,
		JoinMatchFull = 1214,
		JoinNoMatch = 1215,
		MatchmakingNotWaiting = 1220,
		MatchmakingInMatch = 1221,
		MatchmakingNotMatchmaking = 1222,
		UserNotInMatchmaking = 1223,
		QuitNoMatch = 1230,
		QuitNotInMatch = 1231,
		AbortNoMatch = 1240,
		AbortNotInMatch = 1241,
		AbortNotLoading = 1242,
		LoadNotLoading = 1270,
		LoadNoMatch = 1271,
		LoadNotInMatch = 1272,

		ActionMatchNotFound = 1400,
		ActionMatchNotPlaying = 1401,

		TestNoMatch = 1900,
	}
}