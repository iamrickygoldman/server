publish:
dotnet publish --self-contained --runtime linux-x64

run:
dotnet run
dotnet run test

build:
dotnet build

add package:
dotnet add package <name>

close port:
fuser -k 8080/tcp