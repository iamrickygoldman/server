﻿using System;
using System.Threading;

using HanzoNetwork;

namespace ConsoleApplication
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Server Started!");

        	Server server = new Server();
            if (args.Length > 0)
            {
                if (args[0] == "test")
                {
                    server.Testing = true;
                }
            }

        	Thread serverThread = new Thread(() => 
        	{
        		Thread.CurrentThread.IsBackground = true;

        		server.StartListening();

        	});
            serverThread.Start();

            Thread matchmakingThread = new Thread(() =>
            {
                //Thread.CurrentThread.IsBackground = true;

                server.StartMatchmaking();
            });
            matchmakingThread.Start();

            /*
        	Thread quitThread = new Thread(() =>
        	{
        		Console.WriteLine("Type q to quit.");
	        	while (true)
	        	{
	        		if (Console.ReadLine() == "q")
	        		{
	        			server.Close();
	        			return;
	        		}
	        	}
        	});
        	quitThread.Start();
            */
        }
    }
}
