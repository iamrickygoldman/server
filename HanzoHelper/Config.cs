using System;
using System.Net;
using System.IO;

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.FileExtensions;
using Microsoft.Extensions.Configuration.Json;

namespace HanzoHelper
{
	public class Config
	{
		private IConfigurationRoot configuration;

		public Config()
		{
			var builder = new ConfigurationBuilder()
        		.SetBasePath(Directory.GetCurrentDirectory())
        		.AddJsonFile("configuration.json");
        	configuration = builder.Build();
		}

		public IPAddress GetNetworkAddress()
		{
			return IPAddress.Parse(configuration.GetSection("Network").GetSection("Address").Value);
		}

		public int GetNetworkPort()
		{
			return Int32.Parse(configuration.GetSection("Network").GetSection("Port").Value);
		}

		public string GetNetworkShutdownPassword()
		{
			return configuration.GetSection("Network").GetSection("ShutdownPassword").Value;
		}

		public string GetDatabaseAddress()
		{
			return configuration.GetSection("Database").GetSection("Address").Value;
		}

		public string GetDatabaseUser()
		{
			return configuration.GetSection("Database").GetSection("User").Value;
		}

		public string GetDatabasePassword()
		{
			return configuration.GetSection("Database").GetSection("Password").Value;
		}

		public string GetDatabaseDatabase()
		{
			return configuration.GetSection("Database").GetSection("Database").Value;
		}
	}
}