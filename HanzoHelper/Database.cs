using System;
using System.Net;
using System.Collections.Generic;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;

using HanzoNetwork;

namespace HanzoHelper
{
	public class Database
	{
		private MySqlConnection Connection {get; set;}
		private MySqlDataReader Reader {get; set;}

		public Database()
		{
			Config config = new Config();
			string details = "server="+config.GetDatabaseAddress()+";userid="+config.GetDatabaseUser()+";password="+config.GetDatabasePassword()+";database="+config.GetDatabaseDatabase();
			Connection = new MySqlConnection(details);
		}

		public bool ValidateLogin(int id, string token, int mmr)
		{
			bool success = false;
			string weekAgo = DateTime.Today.AddDays(-7).ToString("yyyy-MM-dd HH:mm:ss");
			try {
	            Connection.Open();
	            string query = "SELECT EXISTS(SELECT 1 FROM users WHERE id = "+id+" AND api_token = '"+token+"' AND api_token_refreshed > '"+weekAgo+"' AND mmr = "+mmr+" LIMIT 1)";
	            MySqlCommand command = new MySqlCommand(query, Connection);
	            Reader = command.ExecuteReader();
	            while (Reader.Read()) 
	            {
	            	success = Reader.GetBoolean(0);
	            }
	        } catch (MySqlException e) {
	            Console.WriteLine("Error: {0}",  e.ToString());
	        } finally {
	            if (Reader != null) 
	            {
	                Reader.Close();
	            }
	            if (Connection != null) 
	            {
	                Connection.Close();
	            }
	        }
	        return success;
		}

		public long InsertConnection(IPAddress ip, int port)
		{
			long index = -1;
			string now = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss");
			try {
	            Connection.Open();
	            string query = "INSERT into server_connections(ip,port,time_connected) values('" +ip.ToString()+ "','" +port+ "','" +now+ "');";  
	            MySqlCommand command = new MySqlCommand(query, Connection);
	            Reader = command.ExecuteReader();
	            while (Reader.Read()) 
	            {
	            }
	            index = command.LastInsertedId;
	        } catch (MySqlException e) {
	            Console.WriteLine("Error: {0}",  e.ToString());
	        } finally {
	            if (Reader != null) 
	            {
	                Reader.Close();
	            }
	            if (Connection != null) 
	            {
	                Connection.Close();
	            }
	        }
	        return index;
		}

		public bool AddUserToConnection(User user)
		{
			if (user.ConnectionIndex < 0)
			{
				return false;
			}
			bool result = true;
			string now = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss");
			try {
	            Connection.Open();
	            string query = "UPDATE server_connections SET user_id=" +user.Id+ " WHERE id=" +user.ConnectionIndex+";";
	            MySqlCommand command = new MySqlCommand(query, Connection);
	            Reader = command.ExecuteReader();
	            while (Reader.Read()) 
	            {
	            }
	        } catch (MySqlException e) {
	            Console.WriteLine("Error: {0}",  e.ToString());
	            result = false;
	        } finally {
	            if (Reader != null) 
	            {
	                Reader.Close();
	            }
	            if (Connection != null) 
	            {
	                Connection.Close();
	            }
	        }
	        return result;
		}

		public bool EndConnection(long index)
		{
			bool result = true;
			string now = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss");
			try {
	            Connection.Open();
	            string query = "UPDATE server_connections SET time_disconnected='" +now+ "' WHERE id=" +index+";";
	            MySqlCommand command = new MySqlCommand(query, Connection);
	            Reader = command.ExecuteReader();
	            while (Reader.Read()) 
	            {
	            }
	        } catch (MySqlException e) {
	            Console.WriteLine("Error: {0}",  e.ToString());
	            result = false;
	        } finally {
	            if (Reader != null) 
	            {
	                Reader.Close();
	            }
	            if (Connection != null) 
	            {
	                Connection.Close();
	            }
	        }
	        return result;
		}

		public bool EndAllConnections()
		{
			bool result = true;
			string now = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss");
			try {
	            Connection.Open();
	            string query = "UPDATE server_connections SET time_disconnected='" +now+ "' WHERE time_disconnected IS NULL;";
	            MySqlCommand command = new MySqlCommand(query, Connection);
	            Reader = command.ExecuteReader();
	            while (Reader.Read()) 
	            {
	            }
	        } catch (MySqlException e) {
	            Console.WriteLine("Error: {0}",  e.ToString());
	            result = false;
	        } finally {
	            if (Reader != null) 
	            {
	                Reader.Close();
	            }
	            if (Connection != null) 
	            {
	                Connection.Close();
	            }
	        }
	        return result;
		}

		public List<string> GetBlacklist(string type)
		{
			string result = "";
			try {
	            Connection.Open();
	            string query = "SELECT list FROM ip_blocks WHERE type = '"+type+"';";
	            MySqlCommand command = new MySqlCommand(query, Connection);
	            Reader = command.ExecuteReader();
	            while (Reader.Read()) 
	            {
	            	result = Reader.GetString(0);
	            }
	        } catch (MySqlException e) {
	            Console.WriteLine("Error: {0}",  e.ToString());
	        } finally {
	            if (Reader != null) 
	            {
	                Reader.Close();
	            }
	            if (Connection != null) 
	            {
	                Connection.Close();
	            }
	        }
	        try {
	        	List<string> data = JsonConvert.DeserializeObject<List<string>>(result);
	        	return data;
        	} catch (Exception e) {
        		Console.WriteLine("Exception: {0}", e.ToString());
        		return new List<string>();
        	}
		}
	}
}